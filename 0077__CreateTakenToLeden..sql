Use ModernWays;

drop table if exists TakenToLeden;
CREATE TABLE TakenToLeden(Taken_Id int not null, Leden_Id int not null);

alter table TakenToLeden
add FOREIGN KEY fk_TakenToLeden_Taken(Taken_Id) REFERENCES Taken(Id),
add FOREIGN KEY fk_TakenToLeden_Leden(Leden_Id) REFERENCES Leden(Id);