USE ModernWays;

DROP TABLE IF EXISTS `Postcodes`;

CREATE TABLE Postcodes(
    Code CHAR(4),
    Plaats NVARCHAR(50),
    Localite NVARCHAR(50),
    Provincie NVARCHAR(50),
    Provence NVARCHAR(50)
);