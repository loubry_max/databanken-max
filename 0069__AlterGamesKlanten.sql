use modernways; 

alter table Klanten
modify column personen_id int not null;

alter table Games
modify column personen_id int not null;