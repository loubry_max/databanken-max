DROP DATABASE IF EXISTS ModernWays;
DROP DATABASE IF EXISTS modernways;
CREATE DATABASE ModernWays;
USE ModernWays;
-- MySQL dump 10.13  Distrib 8.0.15, for Linux (x86_64)
--
-- Host: localhost    Database: ModernWays
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Auteurs`
--

DROP TABLE IF EXISTS `Auteurs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Auteurs` (
  `Voornaam` varchar(25) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Familienaam` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Auteurs`
--

LOCK TABLES `Auteurs` WRITE;
/*!40000 ALTER TABLE `Auteurs` DISABLE KEYS */;
INSERT INTO `Auteurs` VALUES ('Samuel','Ijsseling',1),('Jacob','Van Sluis',2),('Emile','Benveniste',3),('Evert W.','Beth',4),('R├®my','Bernard',5),('Robert','Bly',6),('timothy','gowers',7),(NULL,'?',8),(NULL,'Ovidius',9),('Haruki','Murakami',10),('David','Mitchell',11),('Nick','Harkaway',12),('Thomas','Ligotti',13);
/*!40000 ALTER TABLE `Auteurs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Boeken`
--

DROP TABLE IF EXISTS `Boeken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Boeken` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Titel` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Stad` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Uitgeverij` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Verschijningsdatum` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Herdruk` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Commentaar` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Categorie` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Auteurs_Id` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_Boeken_Auteurs` (`Auteurs_Id`),
  CONSTRAINT `fk_Boeken_Auteurs` FOREIGN KEY (`Auteurs_Id`) REFERENCES `Auteurs` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Boeken`
--

LOCK TABLES `Boeken` WRITE;
/*!40000 ALTER TABLE `Boeken` DISABLE KEYS */;
INSERT INTO `Boeken` VALUES (1,'Heidegger. Denken en Zijn. Geven en Danken','Amsterdam','','2014','','BOEIEND!','Filosofie',1),(2,'Lees wijzer bij Zijn en Tijd','Amsterdam','Budel','1998','','BOEIEND!','Filosofie',2),(3,'Le vocabulaire des institutions Indo-Europ├®ennes','Paris?','Les ├®ditions de minuit','1969','?','Een goed geschiedenis boek','Linguistiek',3),(4,'De Wijsbegeerte der Wiskunde. Van Parmenides tot Bolzano','Amsterdam','Philosophische Biliotheek Uitgeversmij. N.V. Standaard-Boekhandel','1944','?','BOEIEND!','Filosofie',4),(6,'Antonin le Pieux. Le si├¿cle d\'or de Rome 138-161','?','Librairie Arthme Fayard','2005','?','Een goed boek','Geschiedenis',5),(9,'The sibling society','Londen','Persus','1996','?','Een interessant boek','Antropologie',6),(12,'Wiskunde, de nog kortere introductie','Utrecht','Uitgeverij Het Spectrum B.V.','2004','?','Oorpronkelijke titel: Mathematics a very schort introduction. Oxford University Press, 2002','Wiskunde',7),(13,'Wiskunde voor krankzinnigen!','Utrecht','Uitgeverij Het Spectrum B.V.','2005','?','Oorpronkelijke titel: Mathematics a very schort introduction. Oxford University Press, 2003','Wiskunde',7),(17,'Beowulf',NULL,NULL,'0975',NULL,NULL,NULL,8),(18,'Metamorfosen',NULL,NULL,'8',NULL,NULL,NULL,9),(19,'Kafka on the Shore',NULL,NULL,NULL,NULL,NULL,NULL,10),(20,'Norwegian Wood',NULL,NULL,NULL,NULL,NULL,NULL,10),(21,'1Q84',NULL,NULL,NULL,NULL,NULL,NULL,10),(22,'Hard-Boiled Wonderland and the End of the World',NULL,NULL,NULL,NULL,NULL,NULL,10),(23,'After the Quake',NULL,NULL,NULL,NULL,NULL,NULL,10),(24,'Cloud Atlas',NULL,NULL,NULL,NULL,NULL,NULL,11),(25,'Number9Dream',NULL,NULL,NULL,NULL,NULL,NULL,11),(26,'The 1000 Autumns of Jacob De Zoet',NULL,NULL,NULL,NULL,NULL,NULL,11),(27,'The Gone-Away World',NULL,NULL,NULL,NULL,NULL,NULL,12),(28,'Angelmaker',NULL,NULL,NULL,NULL,NULL,NULL,12),(29,'Teatro Grottesco',NULL,NULL,NULL,NULL,NULL,NULL,13),(30,'Teatro Grottesco',NULL,NULL,NULL,NULL,NULL,NULL,13);
/*!40000 ALTER TABLE `Boeken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Games`
--

DROP TABLE IF EXISTS `Games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Games` (
  `Title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Developer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Personen_Id` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_Games_Personen` (`Personen_Id`),
  CONSTRAINT `fk_Games_Personen` FOREIGN KEY (`Personen_Id`) REFERENCES `Personen` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Games`
--

LOCK TABLES `Games` WRITE;
/*!40000 ALTER TABLE `Games` DISABLE KEYS */;
INSERT INTO `Games` VALUES ('Doom','ID Software',1,1),('Doom','ID Software',2,2),('Tomb Raider','Crystal Dynamics',3,3),('Tomb Raider','Crystal Dynamics',4,4),('God of War','SCE Santa Monica',5,5),('God of War','SCE Santa Monica',6,6);
/*!40000 ALTER TABLE `Games` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Klanten`
--

DROP TABLE IF EXISTS `Klanten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Klanten` (
  `Spaarpunten` int(11) DEFAULT NULL,
  `Personen_Id` int(11) NOT NULL,
  KEY `fk_klanten_Personen` (`Personen_Id`),
  CONSTRAINT `fk_klanten_Personen` FOREIGN KEY (`Personen_Id`) REFERENCES `Personen` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Klanten`
--

LOCK TABLES `Klanten` WRITE;
/*!40000 ALTER TABLE `Klanten` DISABLE KEYS */;
INSERT INTO `Klanten` VALUES (100,1),(50,7);
/*!40000 ALTER TABLE `Klanten` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Leden`
--

DROP TABLE IF EXISTS `Leden`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Leden` (
  `Voornaam` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Leden`
--

LOCK TABLES `Leden` WRITE;
/*!40000 ALTER TABLE `Leden` DISABLE KEYS */;
INSERT INTO `Leden` VALUES ('Yannick',1),('Bavo',2),('Max',3);
/*!40000 ALTER TABLE `Leden` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Personen`
--

DROP TABLE IF EXISTS `Personen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Personen` (
  `Voornaam` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `Familienaam` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Personen`
--

LOCK TABLES `Personen` WRITE;
/*!40000 ALTER TABLE `Personen` DISABLE KEYS */;
INSERT INTO `Personen` VALUES ('Toon','Kennes','toonkennis@example.com',1),('Patrick','De Wolf','patrickdewolf@example.com',2),('Vincent','Somers','vincentsomers@example.com',3),('Max','Loubry','maxloubry@example.com',4),('Adem','Cetinel','ademcetinel@example.com',5),('Nash','Muylle','nashmuylle@example.com',6),('Bert','De Bleser','bertdebleser@example.com',7);
/*!40000 ALTER TABLE `Personen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Postcodes`
--

DROP TABLE IF EXISTS `Postcodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Postcodes` (
  `Code` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Plaats` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Localite` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Provincie` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Province` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Postcodes`
--

LOCK TABLES `Postcodes` WRITE;
/*!40000 ALTER TABLE `Postcodes` DISABLE KEYS */;
INSERT INTO `Postcodes` VALUES ('2800','Mechelen','Malines','Antwerpen','Anvers'),('3000','Leuven','Louvain','Vlaams Brabant','Brabant Flamand');
/*!40000 ALTER TABLE `Postcodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Taken`
--

DROP TABLE IF EXISTS `Taken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Taken` (
  `Omschrijving` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Taken`
--

LOCK TABLES `Taken` WRITE;
/*!40000 ALTER TABLE `Taken` DISABLE KEYS */;
INSERT INTO `Taken` VALUES ('bestek voorzien',1),('frisdrank meebrengen',2),('aardappelsla maken',3);
/*!40000 ALTER TABLE `Taken` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-08  9:28:00

DROP TABLE IF EXISTS Klanten;
DROP TABLE IF EXISTS Games;
DROP TABLE IF EXISTS Personen;

CREATE TABLE IF NOT EXISTS Platformen(Naam varchar(20) NOT NULL, Id int auto_increment PRIMARY KEY);
INSERT INTO Platformen(Naam)
VALUES
('PS4'),
('Xbox One'),
('Windows'),
('Nintendo Switch'),
('Master System');

CREATE TABLE Games(Titel varchar(100) NOT NULL, Id int auto_increment primary key);
INSERT INTO Games(Titel)
Values
('Anthem'),
('Sekiro: Shadows Die Twice'),
('Devil May Cry 5'),
('Mega Man 11'),
('Oregon Trail');

CREATE TABLE Releases(Games_Id int not null, Platformen_Id int not null);
ALTER TABLE Releases
ADD FOREIGN KEY fk_Releases_Games(Games_Id)
REFERENCES Games(Id);
ALTER TABLE Releases
ADD FOREIGN Key fk_Releases_Platformen(Platformen_Id)
REFERENCES Platformen(Id);

INSERT INTO Releases(Games_Id,Platformen_Id)
values
(1,1),
(1,2),
(1,3),
(2,1),
(2,2),
(2,3),
(3,1),
(3,2),
(4,1),
(4,2),
(4,3),
(4,4);

DROP TABLE Boeken;
CREATE TABLE Boeken(Titel VARCHAR(200),Id int auto_increment primary key);
INSERT INTO Boeken(Titel)
VALUES
('Norwegian Wood'),
('Kafka on the Shore'),
('American Gods'),
('The Ocean at the End of the Lane'),
('Pet Sematary'),
('Good Omens'),
('The Talisman');

INSERT INTO Auteurs(Voornaam,Familienaam)
VALUES
('Neil','Gaiman'),
('Stephen','King'),
('Terry','Pratchett'),
('Peter','Straub');
