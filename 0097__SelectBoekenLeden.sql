use modernways;

SELECT leden.Voornaam, boeken.Titel
FROM uitleningen
     INNER JOIN leden ON uitleningen.leden_Id = leden.Id
     INNER JOIN boeken ON uitleningen.boeken_Id = boeken.Id
