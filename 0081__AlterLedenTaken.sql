Use ModernWays;

Alter TABLE Leden
Drop foreign key fk_Leden_Taken,
Drop column Taken_Id;

Alter TABLE Taken
ADD column Leden_Id int,
ADD foreign key fk_Taken_Leden(Leden_Id) References Leden(Id);