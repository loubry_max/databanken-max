Use ModernWays;

Create Table Uitleningen(
Id int AUTO_INCREMENT PRIMARY KEY,
Leden_Id INT,
Boeken_Id INT,
Startdatum DATE,
Einddatum DATE
);

Alter table Uitleningen
ADD CONSTRAINT fk_Leden_Boeken
FOREIGN KEY (Boeken_Id)
References Boeken(Id);
