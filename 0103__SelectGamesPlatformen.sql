Use ModernWays;

SELECT Platformen.Naam
FROM Platformen 
LEFT JOIN Releases
ON Platformen.Id = Releases.Platformen_Id
WHERE Releases.Platformen_Id is null
UNION
SELECT Games.Titel
FROM Games 
LEFT JOIN Releases
ON Games.Id = Releases.Games_Id
WHERE Releases.Games_Id is null