Use ModernWays;

update Games, Personen
set Personen_Id = Personen.Id
where Games.Voornaam = Personen.Voornaam AND
			Games.Familienaam = Personen.Familienaam AND
            Games.Email = Personen.Email;