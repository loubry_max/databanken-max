drop database if exists `ModernWays`;
create database `ModernWays`;

USE ModernWays;
CREATE TABLE Boeken
(
   Id INTEGER NOT NULL PRIMARY KEY,
   Voornaam VARCHAR(25) NULL,
   Familienaam VARCHAR(50) NOT NULL,
   Titel VARCHAR(255) NOT NULL,
   Stad VARCHAR(100) NULL,
   Uitgeverij VARCHAR(100) NULL,
   Verschijningsdatum CHAR(4) NULL,
   Herdruk CHAR(4) NULL,
   Commentaar VARCHAR(500) NULL,
   Categorie VARCHAR(40) NULL
);

CREATE TABLE Postcodes(
	Code CHAR(4),
    Plaats NVARCHAR(120),
    Localite NVARCHAR(120),
    Provincie NVARCHAR(120),
    Province NVARCHAR(120)
);
