use ModernWays;

alter table Leden
add column Taken_id int,
add foreign key fk_Leden_Taken(Taken_id) references Taken(id);