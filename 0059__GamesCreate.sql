USE ModernWays;
drop table if exists Game;
CREATE TABLE Game (
    Id int AUTO_INCREMENT PRIMARY KEY,
    Titel varchar(255) NOT NULL,
    Ontwikkelaar varchar(255) NOT NULL
);