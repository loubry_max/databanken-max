DROP DATABASE IF EXISTS `modernways`;
CREATE DATABASE `modernways` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;
USE `modernways`;
-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: modernways
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `boeken`
--

DROP TABLE IF EXISTS `boeken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `boeken` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Voornaam` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Familienaam` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Titel` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Stad` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Uitgeverij` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Verschijningsdatum` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Herdruk` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Commentaar` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Categorie` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `boeken`
--

LOCK TABLES `boeken` WRITE;
/*!40000 ALTER TABLE `boeken` DISABLE KEYS */;
INSERT INTO `boeken` VALUES (1,'Samuel','Ijsseling','Heidegger. Denken en Zijn. Geven en Danken','Amsterdam','','2014','','BOEIEND!','Filosofie'),(2,'Jacob','Van Sluis','Lees wijzer bij Zijn en Tijd','Amsterdam','Budel','1998','','BOEIEND!','Filosofie'),(3,'Emile','Benveniste','Le vocabulaire des institutions Indo-Europ├®ennes','Paris?','Les ├®ditions de minuit','1969','?','Een goed geschiedenis boek','Linguistiek'),(4,'Evert W.','Beth','De Wijsbegeerte der Wiskunde. Van Parmenides tot Bolzano','Amsterdam','Philosophische Biliotheek Uitgeversmij. N.V. Standaard-Boekhandel','1944','?','BOEIEND!','Filosofie'),(6,'R├®my','Bernard','Antonin le Pieux. Le si├¿cle d\'or de Rome 138-161','?','Librairie Arthme Fayard','2005','?','Een goed boek','Geschiedenis'),(9,'Robert','Bly','The sibling society','Londen','Persus','1996','?','Een interessant boek','Antropologie'),(12,'timothy','gowers','Wiskunde, de nog kortere introductie','Utrecht','Uitgeverij Het Spectrum B.V.','2004','?','Oorpronkelijke titel: Mathematics a very schort introduction. Oxford University Press, 2002','Wiskunde'),(13,'TiMoThY','GoWeRs','Wiskunde voor krankzinnigen!','Utrecht','Uitgeverij Het Spectrum B.V.','2005','?','Oorpronkelijke titel: Mathematics a very schort introduction. Oxford University Press, 2003','Wiskunde'),(17,NULL,'?','Beowulf',NULL,NULL,'0975',NULL,NULL,NULL),(18,NULL,'Ovidius','Metamorfosen',NULL,NULL,'8',NULL,NULL,NULL),(19,'Haruki','Murakami','Kafka on the Shore',NULL,NULL,NULL,NULL,NULL,NULL),(20,'Haruki','Murakami','Norwegian Wood',NULL,NULL,NULL,NULL,NULL,NULL),(21,'Haruki','Murakami','1Q84',NULL,NULL,NULL,NULL,NULL,NULL),(22,'Haruki','Murakami','Hard-Boiled Wonderland and the End of the World',NULL,NULL,NULL,NULL,NULL,NULL),(23,'Haruki','Murakami','After the Quake',NULL,NULL,NULL,NULL,NULL,NULL),(24,'David','Mitchell','Cloud Atlas',NULL,NULL,NULL,NULL,NULL,NULL),(25,'David','Mitchell','Number9Dream',NULL,NULL,NULL,NULL,NULL,NULL),(26,'David','Mitchell','The 1000 Autumns of Jacob De Zoet',NULL,NULL,NULL,NULL,NULL,NULL),(27,'Nick','Harkaway','The Gone-Away World',NULL,NULL,NULL,NULL,NULL,NULL),(28,'Nick','Harkaway','Angelmaker',NULL,NULL,NULL,NULL,NULL,NULL),(29,'Thomas','Ligotti','Teatro Grottesco',NULL,NULL,NULL,NULL,NULL,NULL),(30,'Thomas','Ligotti','Teatro Grottesco',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `boeken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `games`
--

DROP TABLE IF EXISTS `games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `games` (
  `Title` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Developer` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Voornaam` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Familienaam` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Email` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `games`
--

LOCK TABLES `games` WRITE;
/*!40000 ALTER TABLE `games` DISABLE KEYS */;
INSERT INTO `games` VALUES ('Doom','ID Software',1,'Toon','Kennes','toonkennis@example.com'),('Doom','ID Software',2,'Patrick','De Wolf','patrickdewolf@example.com'),('Tomb Raider','Crystal Dynamics',3,'Vincent','Somers','vincentsomers@example.com'),('Tomb Raider','Crystal Dynamics',4,'Max','Loubry','maxloubry@example.com'),('God of War','SCE Santa Monica',5,'Adem','Cetinel','ademcetinel@example.com'),('God of War','SCE Santa Monica',6,'Nash','Muylle','nashmuylle@example.com');
/*!40000 ALTER TABLE `games` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `klanten`
--

DROP TABLE IF EXISTS `klanten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `klanten` (
  `Voornaam` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Familienaam` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Email` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Spaarpunten` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `klanten`
--

LOCK TABLES `klanten` WRITE;
/*!40000 ALTER TABLE `klanten` DISABLE KEYS */;
INSERT INTO `klanten` VALUES ('Toon','Kennes','toonkennis@example.com',100);
INSERT INTO `klanten` VALUES ('Bert','De Bleser','bertdebleser@example.com',50);
/*!40000 ALTER TABLE `klanten` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `postcodes`
--

DROP TABLE IF EXISTS `postcodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `postcodes` (
  `Code` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `Plaats` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Localite` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Provincie` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Province` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `postcodes`
--

LOCK TABLES `postcodes` WRITE;
/*!40000 ALTER TABLE `postcodes` DISABLE KEYS */;
INSERT INTO `postcodes` VALUES ('2800','Mechelen','Malines','Antwerpen','Anvers'),('3000','Leuven','Louvain','Vlaams Brabant','Brabant Flamand');
/*!40000 ALTER TABLE `postcodes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-23 14:13:26
