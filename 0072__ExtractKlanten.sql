use modernways;
drop table if exists Auteurs;

-- stap1
create table Auteurs(
    Voornaam varchar(255),
    Familienaam varchar(255),
    Id int auto_increment PRIMARY KEY
);

-- stap 2
insert into Auteurs(Voornaam, Familienaam)
select Distinct Voornaam, Familienaam
from Boeken;

-- stap 3
alter table Boeken 
add column Auteurs_Id int, 
add FOREIGN KEY fk_Boeken_Auteurs(Auteurs_Id) 
REFERENCES Auteurs(Id);

-- stap 4
update Boeken, Auteurs 
set Auteurs_Id = Auteurs.Id 
where ((Auteurs.Voornaam is null AND Boeken.Voornaam is null) or Auteurs.Voornaam = Boeken.Voornaam) AND      
((Auteurs.Familienaam is null AND Boeken.Familienaam is null) or Auteurs.Familienaam = Boeken.Familienaam);     

-- stap 5
alter table Boeken
modify column Auteurs_id int not null;

-- stap 6
alter table Boeken
drop Voornaam,
drop Familienaam;