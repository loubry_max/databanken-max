use ModernWays;

Insert into postcodes (
	Code,
    Plaats,
    Localite,
    Provincie,
    Provinceboeken
)
values
('2800','Mechelen', 'Malines', 'Antwerpen', 'Anvers'),
('3000', 'Leuven', 'Louvain', 'Vlaams-Brabant', 'Brabant flamand');