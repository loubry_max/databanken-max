Use ModernWays;

update Klanten, Personen
set Personen_Id = Personen.Id
where Klanten.Voornaam = Personen.Voornaam AND
			Klanten.Familienaam = Personen.Familienaam AND
            Klanten.Email = Personen.Email;