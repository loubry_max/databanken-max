use modernways;

create table BoekenNaarAuteurs

select auteurs.Voornaam, boeken.Titel
from boeken
INNER JOIN auteurs on boeken.id = auteurs.id
