use ModernWays;

alter table Tweets
add column Users_Id int not null,
add foreign key fk_Tweets_Users(Users_Id)
References Users(Id);