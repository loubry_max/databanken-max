Use ModernWays;

SELECT Games.Titel, Platformen.Naam
FROM Releases 
RIGHT JOIN Games ON Releases.Games_Id = Games.Id
LEFT JOIN Platformen ON Platformen.Id = Releases.Platformen_Id